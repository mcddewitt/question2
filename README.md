# Question 2 - David DeWitt
This example build upon the first and adds some configuration methods.


To start:
run npm start from top directory                                     
Server defaults to localhost:5000  

Login:

POST /api/v1/login HTTP/1.1

Value pass:
{
username: username, password: password
}

The returned token can be simply placed in the Authorization header

Authorization: <token>


Logout:
POST /api/v1/logout HTTP/1.1


#Part 2 Items

Get all configurations:

GET /api/v1/configuration HTTP/1.1

Add a configuration 


POST /api/v1/configuration HTTP/1.1

{
	name: name,
	host: host,
	port: port,
	username: username,
}

PUT /api/v1/configuration/<name> HTTP/1.1
{
	host: host,
	port: port,
	username: username,
}

Since I used the name as a ID like field a put cannot update name in this instance.

Remove a configuration:

DELETE /api/v1/configuration/<name> HTTP/1.1

Deletes a configuration based on name.

