'use strict';

const utils = require('../utils');
const crypto = require('crypto');


/**
 * @api {post} /api/v1/login Login to app
 * 
 * @apiParam {username} the username
 * @apiParam {password} the password
 *
 * @apiSuccess {string} auth token
 */

function login(req, res) {
  const reqData = req.body;
  let user = req.app['users'].find((u) => {
    return u.userName === reqData.userName;
  });

  if (user) {
    utils.checkPassword(user, reqData.password, (status) => {
      if (status) {
        //Generate token for authenticating subsequent requests.
        crypto.randomBytes(48, (err, buffer) => {
          let token = buffer.toString('hex');
          user['token'] = token;
          
          res.json(
            { 
              result: 'success',
              token: token 
            }
          );    
        });
      } else {
        res.errorJson(401, 1234, 'Authentication Failed', 'Username/Password is incorrect.');
      }
    });
  }
}

/**
 * @api {post} /api/v1/logout Logout of app
 * 
 * @apiSuccess {message} Logged out message
 */
function logout(req, res) {
  utils.checkToken(req.headers.authorization, req.app['users'], (err, user) => {
    if (err) {
      res.errorJson(400, 1321, 'Invalid Token', 'Invalid authorization token provided.');
    }
    if (user) {
      delete user['token'];
      res.json({ 'message': 'User Successfully logged out.' });
    }
  });
}

module.exports = {
  login: login,
  logout: logout
};

